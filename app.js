const bodyParser = require('body-parser');
const express = require('express')
const app = express();
const cors = require('cors');
const rutas = require('./routes/books');
const users = require('./routes/users');
const travels = require('./routes/travels');
const mongoose = require('mongoose');

/*
app.use('/middleware', (req, res, next) =>{
    console.log("Soy un middleware")
    next();
})

app.get('/', (req, res)=>{

    res.send("Hello World")
})

app.post('/', (req, res)=>{

    res.send("Metodo Post")
})


app.post('/middleware', (req, res) =>{
    res.send("Metodo Postman Middleware")
});
*/

app.use(cors());
app.use(bodyParser.json());
app.use('/', rutas);
app.use('/api/user', users);
app.use('/api/travel', travels);

mongoose.connect(
    'mongodb+srv://jerfjair8:Broncos123@cluster0.f9dwqlx.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0'
    , {useNewUrlParser: true, useUnifiedTopology:true})
    .then(()=> console.log('Conexion a MongoDB exitosa'))
    .catch(err => console.error('No de pudo conectar con MongoDB', err))

app.listen(4000);