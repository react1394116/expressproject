const mongoose = require('mongoose');

const TravelSchema = mongoose.Schema({
    name:{
        type: String,
        require: true
    },
    status:{
        type: String
    },
    rate:{
        type: Number
    },
    description:{
        type: String
    },
    travelDate:{
        type: Date
    },
    total:{
        type: Number
    },
    typePlan:{
        type: String
    },
    img:{
        type: String
    }
});

module.exports = mongoose.model('travels', TravelSchema)