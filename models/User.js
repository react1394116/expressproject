const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    name:{
        type: String,
        require: true
    },
    age:{
        type: Number
    },
    email:{
        type: String
    },
    img:{
        type: String
    }
});

module.exports = mongoose.model('users', UserSchema)