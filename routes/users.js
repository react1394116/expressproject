const express = require('express');
const router = express.Router();
const User = require('../models/User');

router.get('/', async(req, res) =>{
    try {
        const users = await User.find()
        res.json(users)
    } catch (error) {
        res.json({
            message:error
        })
    }
})

router.post('/', async(req, res) =>{
    const user = new User({
        name: req.body.name,
        age: req.body.age,
        email: req.body.email,
        img: req.body.img
    })

    try {
        const savedUser = await user.save()
        res.json(savedUser)
    } catch (error) {
        res.json({
            message:error
        })
    }
})

router.patch('/:id', async (req, res) => {
    try {
        const updatedUser = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!updatedUser) {
            return res.status(404).json({ message: 'Usuario no encontrado' });
        }
        res.json(updatedUser);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.delete('/:id', async(req, res) =>{
    User.deleteOne({
        _id: req.params.id})
        .then(data =>{
            res.json(data);
        })
        .catch( e =>{
            res.json({message: e})
        });
});

module.exports = router;