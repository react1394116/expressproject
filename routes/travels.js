const express = require('express');
const router = express.Router();
const Travel = require('../models/Travel');

router.get('/:id', async (req, res) => {
    try {
        const travel = await Travel.findById(req.params.id);      
        if (!travel) {
            return res.status(404).json({ message: 'Viaje no encontrado' });
        }
        res.json(travel);
    } catch (error) {
        res.status(500).json({ message: 'Error al obtener el viaje por ID', error: error.message });
    }
});

router.get('/', async(req, res) =>{
    try {
        const travels = await Travel.find()
        res.json(travels)
    } catch (error) {
        res.json({
            message:error
        })
    }
})

router.post('/', async(req, res) =>{
    const travel = new Travel({
        name: req.body.name,
        status: req.body.status,
        rate: req.body.rate,
        description: req.body.description,
        travelDate: req.body.travelDate,
        total: req.body.total,
        typePlan: req.body.typePlan,
        img: req.body.img
    })

    try {
        const savedTravel = await travel.save()
        res.json(savedTravel)
    } catch (error) {
        res.json({
            message:error
        })
    }
})

router.patch('/:id', async (req, res) => {
    try {
        const updatedTravel = await Travel.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!updatedTravel) {
            return res.status(404).json({ message: 'Viaje no encontrado' });
        }
        res.json(updatedTravel);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.delete('/:id', async(req, res) =>{
    Travel.deleteOne({
        _id: req.params.id})
        .then(data =>{
            res.json(data);
        })
        .catch( e =>{
            res.json({message: e})
        });
});

module.exports = router;